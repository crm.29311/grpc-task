const grpc = require('@grpc/grpc-js');
const { MicroserviceServiceClient } = require('../proto/microservice_grpc_pb');
const { MicroserviceRequest } = require('../proto/microservice_pb');
const http = require('http');

const from = process.argv[2];
const addr = process.argv[3];
const http_server_addr = process.argv[4];

const getData = async (client) => {
	return new Promise((resolve, reject) => {
		const req = new MicroserviceRequest().setFrom(from);
		client.requestData(req, async (err, message) => {
			if (err) reject(err);
			resolve(message.getData());
		});
	});
};

const main = async () => {
	const creds = grpc.ChannelCredentials.createInsecure();
	const client = new MicroserviceServiceClient(addr, creds);
	const server = http.createServer(async (req, res) => {
		if (req.method === 'GET') {
			const data = await getData(client);
			console.log('data: ', data);
			res.end(data);
		}
	});
	server.listen(http_server_addr).on('close', client.close);
};

main();
