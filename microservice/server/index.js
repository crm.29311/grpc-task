const grpc = require('@grpc/grpc-js');
const { MicroserviceServiceService } = require('../proto/microservice_grpc_pb');
const { MicroserviceServiceClient } = require('../proto/microservice_grpc_pb');
const { MicroserviceResponse, MicroserviceRequest } = require('../proto/microservice_pb');

const name = process.argv[2];
const addr = process.argv[3];
const second_endpoint = process.argv[4];

const getDataFromEndpoint = (client) => {
	const req = new MicroserviceRequest().setFrom(name);
	return new Promise((resolve, reject) => {
		client.requestData(req, async (err, message) => {
			if (err) reject(err);
			resolve(message.getData());
		});
	});
};

const cleanup = async (server) => {
	if (server) server.forceShutdown();
};

const requestData = async (call, callback) => {
	let dataToSend;
	let from = call.request.getFrom();
	if (from.startsWith('client')) {
		dataToSend = await getDataFromEndpoint(global.client);
		console.log(`client ${from} request, data: ${dataToSend}`);
	} else if (from.startsWith('endpoint')) {
		dataToSend = global.data;
		console.log(`server ${from} request, data: ${dataToSend}`);
	} else {
		callback(new Error('unknown from request'), null);
	}
	callback(null, new MicroserviceResponse().setData(dataToSend));
};

const main = async () => {
	process.on('SIGINT', () => {
		console.log('Caught interrupt signal');
		cleanup(server);
	});

	const serverCreds = grpc.ServerCredentials.createInsecure();
	const clientCreds = grpc.ChannelCredentials.createInsecure();

	const server = new grpc.Server();

	global.client = new MicroserviceServiceClient(second_endpoint, clientCreds);
	global.data = 'data from ' + name;

	server.addService(MicroserviceServiceService, { requestData });
	server.bindAsync(addr, serverCreds, (err, _) => {
		if (err) {
			return cleanup(server);
		}
		server.start();
	});

	console.log(`Listening on: ${addr}`);
};

main();
