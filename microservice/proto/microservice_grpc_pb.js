// GENERATED CODE -- DO NOT EDIT!

'use strict';
var grpc = require('@grpc/grpc-js');
var microservice_pb = require('./microservice_pb.js');

function serialize_microservice_MicroserviceRequest(arg) {
  if (!(arg instanceof microservice_pb.MicroserviceRequest)) {
    throw new Error('Expected argument of type microservice.MicroserviceRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_microservice_MicroserviceRequest(buffer_arg) {
  return microservice_pb.MicroserviceRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_microservice_MicroserviceResponse(arg) {
  if (!(arg instanceof microservice_pb.MicroserviceResponse)) {
    throw new Error('Expected argument of type microservice.MicroserviceResponse');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_microservice_MicroserviceResponse(buffer_arg) {
  return microservice_pb.MicroserviceResponse.deserializeBinary(new Uint8Array(buffer_arg));
}


var MicroserviceServiceService = exports.MicroserviceServiceService = {
  requestData: {
    path: '/microservice.MicroserviceService/RequestData',
    requestStream: false,
    responseStream: false,
    requestType: microservice_pb.MicroserviceRequest,
    responseType: microservice_pb.MicroserviceResponse,
    requestSerialize: serialize_microservice_MicroserviceRequest,
    requestDeserialize: deserialize_microservice_MicroserviceRequest,
    responseSerialize: serialize_microservice_MicroserviceResponse,
    responseDeserialize: deserialize_microservice_MicroserviceResponse,
  },
};

exports.MicroserviceServiceClient = grpc.makeGenericClientConstructor(MicroserviceServiceService);
