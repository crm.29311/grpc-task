    npm install
    npm run microservice:client_a // localhost:2000
    npm run microservice:server_a

    npm run microservice:client_b // localhost:3000
    npm run microservice:server_b

_if we make get request to client a, it will request his server-endpoint a, then that requests server-endpoint b for data, return it to an a-endpoint and then finally returns data to a client and on the contrary._
